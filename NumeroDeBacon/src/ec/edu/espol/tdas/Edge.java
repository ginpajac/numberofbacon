/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.tdas;

import java.util.Objects;

/**
 *
 * @author Administrador
 */
public class Edge<E> {
    private int peso;
    Vertex<E> orige;
    Vertex<E> destino;
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public Vertex<E> getOrige() {
        return orige;
    }

    public void setOrige(Vertex<E> orige) {
        this.orige = orige;
    }

    public Vertex<E> getDestino() {
        return destino;
    }

    public void setDestino(Vertex<E> destino) {
        this.destino = destino;
    }
    
    public Edge(int peso, Vertex<E> orige, Vertex<E> destino, String no) {
        this.peso = peso;
        this.orige = orige;
        this.nombre=no;
        this.destino = destino;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Edge) ) {
            return false;
        }
        Edge edge= (Edge) obj;
        if(edge.getDestino().equals(this.destino)&& edge.getOrige().equals(this.orige)){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.orige);
        hash = 67 * hash + Objects.hashCode(this.destino);
        return hash;
    }

    @Override
    public String toString() {
        return "("+nombre+")";
    }
    
    
}
