/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.tdas;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Queue;


/**
 *
 * @author Administrador
 */
public class Graph<E> {

    private List<Vertex<E>> vertices;
    private boolean dirigido;

    public Graph(boolean dirigido) {
        this.dirigido = dirigido;
        vertices = new LinkedList<>();

    }
    public void bfs(Vertex<E> v) {
        limpiar();
        Queue<Vertex<E>> cola = new LinkedList<>();
        v.setVisitado(true);
        v.setDistancia(0);
        cola.offer(v);
        while (!cola.isEmpty()) {
            Vertex<E> vi = cola.poll();
            for (Edge<E> e : vi.getArcos()) {
                if (!e.getDestino().isVisitado()) {
                    e.getDestino().setVisitado(true);
                    e.getDestino().setReferencia(vi);
                    e.getDestino().setDistancia(vi.getDistancia()+1);
                    cola.offer(e.getDestino());
                }
            }
        }
    }

    public boolean addVertex(E data) {
        if (data == null || this.contains(data)) {
            return false;
        }
        return vertices.add(new Vertex(data));
    }

    private void dijkstra(Vertex<E> inicio) {
        limpiar();
        inicio.setDistancia(0);
        PriorityQueue<Vertex<E>> cola = new PriorityQueue<>((Vertex<E> v1, Vertex<E> v2) -> v1.getDistancia() - v2.getDistancia());
        cola.offer(inicio);
        while (!cola.isEmpty()) {
            Vertex<E> v = cola.poll();
            for (Edge<E> e : v.getArcos()) {
                Vertex<E> siguiente = e.getDestino();
                int peso = e.getPeso();
                int distancia = v.getDistancia() + peso;
                if (distancia < siguiente.getDistancia()) {
                    cola.remove(siguiente);
                    siguiente.setDistancia(distancia);
                    siguiente.setReferencia(v);
                    cola.offer(siguiente);
                }
            }
        }
    }
    public List<String> caminomasCorto(E origen,E fin, int eleccion){
        LinkedList<String> l=new LinkedList<>();
        if (eleccion==0){
            dijkstra(searhVertex(origen));
        }
        else{
            bfs(searhVertex(origen));
        }
        Vertex<E> v=searhVertex(fin);
        while(v!=null){
            l.addFirst((String) v.getData());
            Vertex<E> prev=v.getReferencia();
            if(prev!=null){
                for(Edge<E> a:prev.getArcos()){
                    if(a.getOrige().equals(prev)&&a.getDestino().equals(v)){
                        l.addFirst(a.getNombre());
                    }
                }
            }
            v=prev;
        }
        return invertirLista(l);
    }
    
    private List<String> invertirLista(LinkedList<String> inver) {
        LinkedList<String> lista = new LinkedList<>();
        ListIterator i = inver.listIterator(inver.size());
        while (i.hasPrevious()) {
            lista.add((String) i.previous());
        }
        return lista;
    }

    public int distanciaMinima(E origen, E destino, int eleccion) {
        Vertex<E> vo = searhVertex(origen);
        Vertex<E> vf = searhVertex(destino);
        if (vo == null || vf == null) {
            return -1;
        }
        if (eleccion==0){
            dijkstra(vo);
        }
        else{
            bfs(vo);
        }
        return vf.getDistancia();
    }

    private boolean contains(E data) {
        for (Vertex<E> v : vertices) {
            if (v.getData().equals(data)) {
                return true;
            }
        }
        return false;
    }

    private Vertex<E> searhVertex(E data) {
        for (Vertex<E> v : vertices) {
            if (v.getData().equals(data)) {
                return v;
            }
        }
        return null;
    }

    public boolean addEdge(E origen, E destino, int peso, String nombre) {
        Vertex<E> vo = searhVertex(origen);
        Vertex<E> vd = searhVertex(destino);
        if (vo == null || vd == null) {
            return false;
        }
        Edge<E> a = new Edge(peso, vo, vd, nombre);
        if (vo.getArcos().contains(a)) {
            return false;
        }
        vo.getArcos().add(a);
        if (!dirigido) {
            Edge<E> b = new Edge(peso, vd, vo, nombre);
            vd.getArcos().add(b);
        }
        return true;
    }

    public boolean isEmpty() {
        return vertices.isEmpty();
    }
    public void limpiar() {
        for (Vertex<E> v : this.vertices) {
            v.setVisitado(false);
            v.setDistancia(Integer.MAX_VALUE);
            v.setReferencia(null);
        }
    }

}
