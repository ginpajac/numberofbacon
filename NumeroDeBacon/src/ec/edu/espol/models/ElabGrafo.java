/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.models;

import ec.edu.espol.tdas.Graph;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grupo 8
 */
public class ElabGrafo {

    HashMap<String, String> peliculas;
    HashMap<String, String> actores;
    HashMap<String, List<String>> arcos;

    public void leerActores() {
        actores = new HashMap<>();
        try (Scanner sc = new Scanner(new File("actores.txt"))) {
            while (sc.hasNextLine()) {
                String linea = sc.nextLine();
                String[] datos = linea.split("\\|");
                actores.put(datos[0], datos[1]);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ElabGrafo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void leerPeliculas() {
        peliculas = new HashMap<>();
        try (Scanner sc = new Scanner(new File("peliculas.txt"))) {
            while (sc.hasNextLine()) {
                String linea = sc.nextLine();
                String[] datos = linea.split("\\|");
                peliculas.put(datos[0], datos[1]);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ElabGrafo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void leerArcos() {
        arcos = new HashMap<>();
        try (Scanner sc = new Scanner(new File("pelicula-actores.txt"))) {
            while (sc.hasNextLine()) {
                String linea = sc.nextLine();
                String []datos = linea.split("\\|");
                if (!arcos.containsKey(datos[0])) {
                    arcos.put(datos[0], new LinkedList<>());
                }
                arcos.get(datos[0]).add(datos[1]);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ElabGrafo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Graph<String> cargarInformacion() {
        leerActores();
        leerPeliculas();
        leerArcos();
        Graph<String> grafo = new Graph<>(false);
        actores.forEach((k, v) ->
            grafo.addVertex(v));
        arcos.forEach((c, p) -> {
            for (String actors : p) {
                for (String falta : morat(actors, p)) {
                    String act = this.actores.get(actors);
                    String fal = this.actores.get(falta);
                    grafo.addEdge(act, fal, 1, this.peliculas.get(c));
                }
            }
        });

        return grafo;
    }

    private List<String> morat(String a, List<String> cortar) {
        List<String> r = cortar;
        ListIterator<String> i = r.listIterator();
        int cnt = 1;
        while (i.hasNext()) {
            String ac = i.next();
            if (ac.equals(a)) {
                return r.subList(cnt, r.size());
            }
            cnt++;
        }
        return r;
    }

}
