/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.models;

import ec.edu.espol.tdas.Graph;
import ec.edu.espol.views.Mostrar;
import java.util.LinkedList;

/**
 *
 * @author Grupo 8
 */
public class Recorrido {
    private boolean bool;
    
    public StringBuilder mostrarPantalla(String a,Graph<String> grafo, int elec) {
        StringBuilder sb = new StringBuilder();
        String actor= capitalizar(a).toString();
        int distancia = grafo.distanciaMinima("Kevin Bacon", actor,elec);
        if (distancia == -1 || distancia == Integer.MAX_VALUE || distancia == 0) {
            bool=false;
            mensajeErroneo(sb, distancia, actor);
            return sb;
        }else{
            bool=true;
        }
        sb.append(distancia).append("\n");
        LinkedList<String> p = (LinkedList<String>) grafo.caminomasCorto("Kevin Bacon", actor,elec);
        Mostrar window=new Mostrar(p);
        window.setVisible(true);
        return sb;
    }
    
    private void mensajeErroneo(StringBuilder sb, int distancia, String actor){
        switch(distancia){
            case -1:
                sb.append("0\n\nNo existe información sobre ").append(actor);
                break;
            case Integer.MAX_VALUE:
                sb.append("0\n\nNo posee relacion con Kevin Bacon");
                break;
            case 0:
                sb.append("0\n\nEs Kevin Bacon");
                break;
            default:
                sb.append("0\n\nOpcion por default");
                break;
        }
    }
    
    public StringBuilder capitalizar(String actor) {
        StringBuilder correcto=new StringBuilder();
        while(actor.startsWith(" ")){
            actor=actor.substring(1);
        }
        String []espacio=actor.split(" ");
        if (actor.isEmpty()) {
            correcto.append(actor);
          return correcto;            
        } else {
            for(int i=0; i<espacio.length;i++){
                correcto.append(espacio[i].substring(0, 1).toUpperCase()).append(espacio[i].substring(1).toLowerCase());
                if(i!=espacio.length-1){
                    correcto.append(" ");
                }
            }
          return correcto; 
        }
    }
    public boolean condicionBoleana(){
        return bool;
    }
    
}
