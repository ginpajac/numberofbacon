/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.views;

import ec.edu.espol.models.ElabGrafo;
import ec.edu.espol.models.Recorrido;
import ec.edu.espol.tdas.Graph;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

/**
 *
 * @author Grupo 8
 */
public class Juego {

    private final BorderPane pane;
    private Button porDijsktra;
    private Button porBFS;
    private TextField texto;
    private Label game;
    private final ElabGrafo e;
    private Ellipse rima;
    private final Graph<String> grafo;
    private final Recorrido bfsDijkstra;

    public Juego() {
        pane = new BorderPane();
        pane.setCenter(generarVBox());
        e = new ElabGrafo();
        grafo = e.cargarInformacion();
        bfsDijkstra=new Recorrido();
        gameOnAction();
    }

    public BorderPane getroot() {
        return pane;
    }

    private VBox generarVBox() {
        VBox vbox = new VBox();
        vbox.setSpacing(20);
        game = new Label();
        rima= imagenes("imagen/flecha.gif", 40);
        rima.setVisible(false);
        Text titulo = new Text("Número de Bacon");
        titulo.setId("title-text");
        vbox.getChildren().addAll(titulo, game,rima,generaLineaTexto(), generabotones());
        vbox.setAlignment(Pos.CENTER);
        return vbox;
    }

    private HBox generabotones() {
        HBox hbox = new HBox();
        porBFS = new Button("Por BFS");
        porDijsktra = new Button("Por Dijsktra");
        porBFS.setMinWidth(120);
        porDijsktra.setMinWidth(120);
        hbox.getChildren().addAll(porDijsktra, porBFS);
        hbox.setSpacing(20);
        hbox.setAlignment(Pos.CENTER);
        return hbox;
    }

    private HBox generaLineaTexto() {
        HBox hbox = new HBox();
        Label label = new Label("Kevin Bacon to: ");
        texto = new TextField();texto.setPrefSize(150, 10);
        hbox.getChildren().addAll(label, texto);
        hbox.setSpacing(20);
        hbox.setAlignment(Pos.CENTER);
        return hbox;
    }

    private void gameOnAction() {
        porDijsktra.setOnAction(event -> {
            
            if (!"".equals(texto.getText())) {
                StringBuilder sb = bfsDijkstra.mostrarPantalla(texto.getText(), grafo,0);
                game.setText(bfsDijkstra.capitalizar(texto.getText()) + " tiene un número de Bacon de " + sb);
                rima.setVisible(bfsDijkstra.condicionBoleana());
            } else {
                game.setText("Inserte el nombre de una persona!");
            }
        });
        porBFS.setOnAction(even -> {
            if (!"".equals(texto.getText())) {
                StringBuilder sb = bfsDijkstra.mostrarPantalla(texto.getText(), grafo,1);
                game.setText(bfsDijkstra.capitalizar(texto.getText()) + " tiene un número de Bacon de " + sb);
                rima.setVisible(bfsDijkstra.condicionBoleana());
            } else {
                game.setText("Inserte el nombre de una persona!");
            }
        });
    }
    
    public Ellipse imagenes(String ruta, double tamano){
        Ellipse e1= new Ellipse(tamano,tamano);
        Image im1= new Image(ruta);
        e1.setFill(new ImagePattern(im1));
        e1.setLayoutX(tamano/2);
        e1.setLayoutY(tamano/2);
        return e1;
    }

}
