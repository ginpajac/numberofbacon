/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.views;

import java.io.FileNotFoundException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Grupo 8
 */
public class Principal extends Application{
    @Override
    public void start(Stage primaryStage) throws FileNotFoundException {
        Stage ventana;
        Scene sceneJuego;
        ventana = primaryStage;
        Juego game = new Juego();
        sceneJuego = new Scene(game.getroot(),540,400);
        sceneJuego.getStylesheets().add(getClass().getResource("miEstilo.css").toExternalForm());
        ventana.getIcons().add(new Image("imagen/ima.jpg"));
        ventana.setTitle("NumberOfBacon");
        ventana.setScene(sceneJuego);
        ventana.show();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);

    }
}
