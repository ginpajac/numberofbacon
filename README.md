# EL JUEGO DEL NUMERO DE BACON

**Historia**

Kevin Bacon, un actor muy reconocido, inspir� a unos universitarios para crear el juego "Six Degrees of
Kevin Bacon", el cual consiste en encontrar el n�mero de bacon de cualquier actor o actriz. Este n�mero
es calculado de acuerdo al m�nimo n�mero de conexiones en pel�culas de un actor cualquiera hasta
llegar al actor Kevin Bacon. Por ejemplo:
La actriz Mary Pickford tiene un n�mero bacon de 3. Ella actu� en la pelicula "In Old Kentucky" con el
actor Mack Sennett, el cual actu� en la pelicula "Down Memory Lane" con Yvonne Peattie, y ella actu�
en la pelicula "The Big Picture" con Kevin Bacon. Por lo que hay tres conexiones que la llevaron hasta
Kevin Bacon.

**Descripcion**

Encontrar el n�mero de Bacon y el camino hasta Kevin Bacon son tareas que pueden ser resueltas
utilizando grafos donde los v�rtices ser�an los actores y los arcos las pel�culas donde actuaron juntos.
Para el desarrollo de este peque�o proyecto, usted deber� encontrar el camino m�s corto entre un
actor dado y Kevin Bacon, en el cual usted aplicara dos algoritmos vistos en clases: Dijkstra y BFS. El
algoritmo BFS usted lo implementara es muy parecido al recorrido en anchura solo que esta vez usted
tendr� que almacenar en el grafo alguna forma para poder obtener la ruta (es un tanto parecido a como
se hizo en dijkstra).


## Ginger Jacome